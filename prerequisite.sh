#!/bin/bash
sudo apt-get update -y
sudo apt-get install nodejs npm -y
sudo apt install git gcc g++ cmake libjsoncpp-dev uuid-dev openssl libssl-dev zlib1g-dev -y

git clone https://github.com/an-tao/drogon
cd drogon
git submodule update --init
mkdir build
cd build
cmake ..
make && sudo make install


git clone https://gitlab.com/Lancey139/phaser-garry-mod.git
cd phaser-garry-mod/

# Front-end
npm install
npm run build
npm run serve

# Back-end
cd backend/
mkdir build && cd build
cmake ../ && make