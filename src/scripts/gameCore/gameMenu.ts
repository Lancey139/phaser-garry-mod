import FrontConf from "../conf";
import BackEndWebSocket from "../network/backEndWebSocket";

export default class GameMenu {
    startGameButton : Phaser.GameObjects.Sprite
    startGameText : Phaser.GameObjects.Text
    positionX : number
    positionY : number
    backEndWebSocket : BackEndWebSocket

    constructor(scene: Phaser.Scene, frontConf : FrontConf, backEnd : BackEndWebSocket) {
        this.positionX = frontConf.width/2;
        this.positionY = frontConf.height - 100

        this.backEndWebSocket = backEnd;

        this.startGameButton = scene.add.sprite(this.positionX, this.positionY, 'menuButton', 'button1');
        this.startGameButton.setInteractive();
        this.startGameButton.setScale(0.7, 0.7);
        this.startGameButton.setOrigin(0.5, 0.5);
        this.startGameButton.on('clicked', (button : Phaser.GameObjects.Sprite) => {
            this.startGameButton.setFrame("button1-clicked");
            this.hideMenu();
            this.backEndWebSocket.startGame();
        });
        this.startGameButton.on('pointerup', (button : Phaser.GameObjects.Sprite) => {
        this.startGameButton.setFrame("button1");
        });

        this.startGameText = scene.add.text(this.positionX , this.positionY, "Start the game");
        this.startGameText.setOrigin(0.5, 0.5);
        this.startGameText.setScale(0.7, 0.7);

        scene.input.on('gameobjectdown', (pointer, gameObject) => {gameObject.emit('clicked', gameObject)});
        scene.input.on('gameobjectup', (pointer, gameObject) => {gameObject.emit('pointerup', gameObject)}); 
    }

    hideMenu() {
        this.startGameButton.setActive(false);
        this.startGameButton.setVisible(false);
        this.startGameText.setVisible(false);
    }

    printMenu() {
        this.startGameButton.setActive(true);
        this.startGameButton.setVisible(true);
        this.startGameText.setVisible(true);
    }
  
    public update(cameraX : number, cameraY : number) {
      this.startGameButton.setX(this.positionX + cameraX);
      this.startGameButton.setY(this.positionY + cameraY);
      this.startGameText.setX(this.positionX + cameraX);
      this.startGameText.setY(this.positionY + cameraY);
    }
  }
  