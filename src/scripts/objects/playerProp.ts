import GameObjectsGroup from "./gameObjetGroup";
import Player, { PlayerInterface } from "./player"

export default class PlayerProp extends Player {
    gameObjectGroup : GameObjectsGroup
    targettedObject : Phaser.Physics.Arcade.Sprite
    isObjectOnRange : boolean

    constructor(scene : Phaser.Scene, startPoint : Phaser.Math.Vector2 , playerAtlas : string, uuid : string,
        gameObjectGroup ?: GameObjectsGroup) {
        super(scene, startPoint, playerAtlas, 'prop-start', uuid);
        if(gameObjectGroup) {
            this.gameObjectGroup = gameObjectGroup;
            this.targettedObject = this.gameObjectGroup.objectArray[0];
        }
        
        this.isObjectOnRange = false;

        // Set size of the initial image
        this.setSize(32, 32);
        this.setOffset(0,0);

        // Bind become object on letter F
        var keyObj = scene.input.keyboard.addKey('F');
        keyObj.on('down', this.onChangeApparence, this);
    }

    update(cursors : Phaser.Types.Input.Keyboard.CursorKeys) {
        super.update(cursors);

        // Check if at least one object is there
        if(this.gameObjectGroup.objectArray.length == 0) {
            console.error("No objects could be found on map");
            return;
        }

        // List objects in range
        let closestObject = this.gameObjectGroup.objectArray[0];
        this.gameObjectGroup.objectArray.forEach( 
            (object: Phaser.Physics.Arcade.Sprite) => {
                if(Math.abs(Phaser.Math.Distance.BetweenPoints(object, this)) < 
                    Math.abs(Phaser.Math.Distance.BetweenPoints(closestObject, this))) {
                        closestObject = object;
                }
            });
        // If closest object is close enough, underlight it and update targetted object
        if(Math.abs(Phaser.Math.Distance.BetweenPoints(closestObject, this)) < 80) {
            this.targettedObject.clearTint();
            this.targettedObject = closestObject;
            this.targettedObject.tint = 0x94E665;
            this.isObjectOnRange = true;
        } else {
            this.targettedObject.clearTint();
            this.isObjectOnRange = false;
        }
    }

    onChangeApparence(event: any) {
        if(this.isObjectOnRange) {
            this.setFrame(this.targettedObject.frame.name);
            this.setSize(this.targettedObject.body.width, this.targettedObject.body.height);
            this.setOffset(0,0);
        }
    }

    toJsonBackEnd() : PlayerInterface {
        let returnValue : PlayerInterface = super.toJsonBackEnd();
        returnValue.role = "prop";
        return returnValue;
    }

    updateFromJson(jsonMessage : PlayerInterface) {
        super.updateFromJson(jsonMessage);        
        if(this.frame.name != jsonMessage.frame) {
            this.setFrame( jsonMessage.frame);
            this.setSize(jsonMessage.width, jsonMessage.height);
            this.setOffset(0,0);
        }
    }
}