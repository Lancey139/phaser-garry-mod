#include <jsoncpp/json/json.h>

#include "PlayerWebsock.h"


void PlayerWebsock::handleNewMessage(const WebSocketConnectionPtr& wsConnPtr, std::string &&message, const WebSocketMessageType &type)
{
    // Parsing new message
    Json::Reader reader;
    Json::Value messageJson;
    std::string response;
    reader.parse(message, messageJson); // reader can also read strings

    std::string gameId;

    // Return game list without registering client
    if(messageJson[MESSAGE_TYPE] == MessageType::LIST_GAME) {
    	LOG_INFO << "Sending list of current games";
    	wsConnPtr->send(listGamesHandler());
    	return;
	}

    // Checking for gameId
    if(messageJson[GAME_ID].asString() != "") {
    	gameId = messageJson[GAME_ID].asString();
    } else {
    	LOG_ERROR << "Message received without game ID" << messageJson.toStyledString();
    	return;
    }

	// Updating uuid / websoket map if message is NEW_PLAYER
    {
    	std::lock_guard lock {m_mutex};
    	std::pair<std::string,std::string> uniqueId {gameId, messageJson[SOCKET_UUID].asString()};
    	if(messageJson[MESSAGE_TYPE] == MessageType::NEW_PLAYER &&
    			m_mapWebSocket.find(uniqueId) == m_mapWebSocket.end()) {
    		LOG_INFO << "Create new socket " << messageJson[SOCKET_UUID].asString() <<" in map - memory adress :" << wsConnPtr.get();
    		m_mapWebSocket[uniqueId] = wsConnPtr;
    	}
    }

	// Handle messages
	if(messageJson[MESSAGE_TYPE] == MessageType::NEW_PLAYER ||
			messageJson[MESSAGE_TYPE] == MessageType::PLAYER_MOVED) {
		response = m_gamesManager.playerHandler(messageJson[MESSAGE_TYPE], gameId, messageJson[PLAYER]);
		wsConnPtr->send(response);
    } else if (messageJson[MESSAGE_TYPE] == MessageType::NEW_BULLET ||
    			messageJson[MESSAGE_TYPE] == MessageType::BULLET_DESTROY) {
    	response = m_gamesManager.bulletHandler(messageJson[MESSAGE_TYPE], gameId, messageJson[BULLET]);
    	// Send message to all other clients from this game
    	sendMessageToPlayersInGame(response, gameId, messageJson[SOCKET_UUID].toStyledString());
	} else if(messageJson[MESSAGE_TYPE] == MessageType::NEW_GAME) {
		response = m_gamesManager.gameCreationHandler(messageJson[MESSAGE_TYPE], gameId, messageJson);
		wsConnPtr->send(response);
	} else if(messageJson[MESSAGE_TYPE] == MessageType::GAME_STATE) {
		m_gamesManager.gameStateHandler(messageJson[MESSAGE_TYPE], gameId, messageJson);
	} else {
    	LOG_WARN << "WARNING : Unknown message received " << message;
    }
}

void PlayerWebsock::handleNewConnection(const HttpRequestPtr &req,const WebSocketConnectionPtr& wsConnPtr)
{
	LOG_INFO << "New connection with socket " << wsConnPtr.get();
}

void PlayerWebsock::handleConnectionClosed(const WebSocketConnectionPtr& wsConnPtr)
{
    //write your application logic here
	LOG_INFO << "Connection closed with socket " << wsConnPtr.get();
	// Destroy associated player
	std::lock_guard lock {m_mutex};
	for (auto it = m_mapWebSocket.begin(); it != m_mapWebSocket.end(); ++it) {
	    if (it->second == wsConnPtr) {
	    	// Found corresponding uuid, deleting corresponding user
	    	LOG_INFO << "Delete player " <<  it->first.second;
	    	m_gamesManager.deletePlayerInGame(it->first.first, it->first.second);
	    	// Delete player in mapWebSocket
	    	LOG_INFO << "Delete socket "<< it->first.second << " in map - memory adress" << it->second.get();
	    	m_mapWebSocket.erase(it);
	    	break;
	    }
	}
}

void PlayerWebsock::sendMessageToPlayersInGame(std::string message, std::string gameId, std::string playerToExcludeId) {
	std::lock_guard lock {m_mutex};
	for(const auto & client : m_mapWebSocket) {
		if(client.first.first.compare(gameId) == 0 && client.first.second.compare(playerToExcludeId) !=0) {
			client.second->send(message);
		}
	}
}

std::string PlayerWebsock::listGamesHandler() {
	const auto & gameList = m_gamesManager.listCurrentGames();
	Json::Value response;
	response[MESSAGE_TYPE] = MessageType::LIST_GAME;
	response["gamelist"] = Json::arrayValue;
	for(const auto & game : gameList) {
		Json::Value gameJson;
		gameJson[GAME_ID] = game.first;
		gameJson[GAME_NAME] = game.second;
		response["gamelist"].append(gameJson);
	}
	return response.toStyledString();
}
