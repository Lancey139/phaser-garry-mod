#pragma once
#include <string.h>

enum MessageType {
    NEW_PLAYER = 1,
    PLAYER_MOVED = 2,
	GAME_STATE = 3,
	PLAYER_DESTROY = 4,
	NEW_BULLET = 5,
	BULLET_DESTROY = 6,
	NEW_GAME = 7,
	LIST_GAME = 8,
	PLAYER_ROLE = 9,
};

const std::string MESSAGE_TYPE = "Type";
const std::string SOCKET_UUID = "SocketUuid";
const std::string PLAYER = "Player";
const std::string PLAYERS = "Players";
const std::string BULLET = "Bullet";
const std::string GAME_ID = "GameId";
const std::string GAME_NAME = "GameName";
const std::string GAME_STATUS = "GameStatus";
