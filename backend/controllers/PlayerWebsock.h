#pragma once
#include <drogon/WebSocketController.h>
#include <mutex>
#include <map>

#include "game/GamesManager.h"
#include "MessageType.h"


using namespace drogon;
class PlayerWebsock:public drogon::WebSocketController<PlayerWebsock>
{
  public:
    virtual void handleNewMessage(const WebSocketConnectionPtr&,
                                  std::string &&,
                                  const WebSocketMessageType &) override;
    virtual void handleNewConnection(const HttpRequestPtr &,
                                     const WebSocketConnectionPtr&)override;
    virtual void handleConnectionClosed(const WebSocketConnectionPtr&)override;
    WS_PATH_LIST_BEGIN
    //list path definitions here;
    //WS_PATH_ADD("/path","filter1","filter2",...);
	WS_PATH_ADD("/game");
    WS_PATH_LIST_END

  private:
	void sendMessageToPlayersInGame(std::string message, std::string gameId, std::string playerToExcludeId);
	std::string listGamesHandler();
	GamesManager m_gamesManager;
	// Map associating game uuid / player uuid to web socket
	std::map<std::pair<std::string, std::string>, WebSocketConnectionPtr> m_mapWebSocket;
	std::mutex m_mutex;

};
