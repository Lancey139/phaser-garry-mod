#include <algorithm>
#include <iostream>
#include <drogon/WebSocketController.h>

#include "GameSession.h"
#include "controllers/MessageType.h"
#include "utils/Utils.h"

GameSession::GameSession(const GameSession & gameSession) {
	std::lock_guard lock {gameSession.m_mutex};
	m_PlayerVector = gameSession.m_PlayerVector;
	m_BulletVector = gameSession.m_BulletVector;
	m_gameName = gameSession.m_gameName;
}

GameSession::GameSession(GameSession && gameSession) {
	std::lock_guard lock {gameSession.m_mutex};
	m_PlayerVector = std::move(gameSession.m_PlayerVector);
	m_BulletVector = std::move(gameSession.m_BulletVector);
	m_gameName = std::move(gameSession.m_gameName);
}

GameSession& GameSession::operator=(const GameSession& gameSession) {
	std::lock_guard lock {gameSession.m_mutex};
	m_PlayerVector = gameSession.m_PlayerVector;
	m_BulletVector = gameSession.m_BulletVector;
	m_gameName = gameSession.m_gameName;
	return *this;
}

void GameSession::addNewPlayer(const Json::Value & jsonMessage) {
	// Lock mutex and add player
	std::lock_guard lock {m_mutex};
	LOG_INFO << "Create player find_if";
	auto playerIt = std::find_if(m_PlayerVector.begin(),
			m_PlayerVector.end(),
			[&jsonMessage](Player player){ return player.uuid.compare(jsonMessage["uuid"].asString()) == 0; });
	if(playerIt == m_PlayerVector.end()) {
		LOG_INFO << "Create player push_back";
		m_PlayerVector.push_back(Player{jsonMessage});
	} else {
		LOG_WARN << "Already existing player is beeing created";
	}
	LOG_INFO << "end AddNewPlayer function";
}

void GameSession::updatePlayerPosition(const Json::Value & jsonMessage) {
	LOG_DEBUG << "updatePlayerPosition";
	// Lock mutex and move player
	std::lock_guard lock {m_mutex};
	auto playerIt = std::find_if(m_PlayerVector.begin(),
			m_PlayerVector.end(),
			[&jsonMessage](Player player){ return player.uuid.compare(jsonMessage["uuid"].asString()) == 0; });
	if(playerIt != m_PlayerVector.end()) {
		playerIt->updateFromJson(jsonMessage);
	} else {
		LOG_WARN << "Non exisiting player's position is beeing updated";
	}
	LOG_DEBUG << "end updatePlayerPosition";
}

void GameSession::destroyPlayer(const std::string & uuid) {
	std::lock_guard lock {m_mutex};
	auto playerIt = std::find_if(m_PlayerVector.begin(),
			m_PlayerVector.end(),
			[&uuid](Player player){ return player.uuid.compare(uuid) == 0; });
	if(playerIt != m_PlayerVector.end()) {
		m_PlayerVector.erase(playerIt);
	} else {
		LOG_WARN << "Non exisiting player is beeing destroyed";
	}
}

bool GameSession::addNewBullet(const Json::Value & jsonMessage) {
	std::lock_guard lock {m_mutex};
	auto bulletIt = std::find_if(m_BulletVector.begin(),
			m_BulletVector.end(),
			[&jsonMessage](Bullet bullet){ return bullet.uuid.compare(jsonMessage["uuid"].asString()) == 0; });
	if(bulletIt == m_BulletVector.end()) {
		m_BulletVector.push_back(Bullet{jsonMessage});
		return true;
	} else {
		LOG_WARN << "Already existing bullet is beeing added";
		return false;
	}
}

bool GameSession::destroyBullet(const std::string & uuid) {
	std::lock_guard lock {m_mutex};
	auto bulletIt = std::find_if(m_BulletVector.begin(),
			m_BulletVector.end(),
			[&uuid](Bullet bullet){ return bullet.uuid.compare(uuid) == 0; });
	if(bulletIt != m_BulletVector.end()) {
		m_BulletVector.erase(bulletIt);
		return true;
	} else {
		LOG_WARN << "Non exisiting bullet is beeing destroyed";
		return false;
	}
}

Json::Value GameSession::sendGameState(const std::string & uuidToExclude) {

	LOG_DEBUG << "sendGameState";
	std::lock_guard lock {m_mutex};
	computeGameState();
	Json::Value returnValue;
	returnValue[PLAYERS] = Json::arrayValue;
	auto playerItToExclude = std::find_if(m_PlayerVector.begin(),
			m_PlayerVector.end(),
			[&uuidToExclude](Player player){ return player.uuid.compare(uuidToExclude) == 0; });
	for(auto itPlayer : m_PlayerVector) {
		if( playerItToExclude == m_PlayerVector.end() || itPlayer.uuid.compare(playerItToExclude->uuid) != 0) {
			returnValue[PLAYERS].append(itPlayer.toJson());
		}
	}

	returnValue[GAME_STATUS] = m_gameStatus.load();
	LOG_DEBUG << "end sendGameState";
	return returnValue;
}

void GameSession::setGameStatus(GameStatus newStatus) {
	LOG_DEBUG << "setGameStatus" << newStatus;
	std::lock_guard lock {m_mutex};
	m_gameStatus = newStatus;
	if(m_gameStatus == GameStatus::STARTING) {
		// Set all players to prop
		std::for_each(m_PlayerVector.begin(), m_PlayerVector.end(), [](Player & player){ player.role = "prop"; });
		// Choose what player will be the hunt
		m_PlayerVector[Utils::select_randomly(0,  m_PlayerVector.size()-1)].role = "hunt";
	}
	LOG_DEBUG << "end setGameStatus";
}

void GameSession::computeGameState() {

}

