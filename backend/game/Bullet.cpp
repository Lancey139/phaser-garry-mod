#include "Bullet.h"

Bullet::Bullet(const Json::Value & jsonMessage) {
	updateFromJson(jsonMessage);
}

void Bullet::updateFromJson(const Json::Value & jsonMessage) {
	uuid = jsonMessage["uuid"].asString();
	startX = jsonMessage["startX"].asInt();
	startY = jsonMessage["startY"].asInt();
	toX = jsonMessage["toX"].asInt();
	toY = jsonMessage["toY"].asInt();
}


Json::Value Bullet::toJson() {
	Json::Value returnValue;
	returnValue["uuid"] = uuid;
	returnValue["startX"] = startX;
	returnValue["startY"] = startY;
	returnValue["toX"] = toX;
	returnValue["toY"] = toY;

	return returnValue;
}
