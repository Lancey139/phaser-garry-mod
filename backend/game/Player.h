#include <string>
#include <jsoncpp/json/json.h>
struct Player {

    Player(const Json::Value & jsonMessage);
    void updateFromJson(const Json::Value & jsonMessage);

    Json::Value toJson();

    inline bool operator==(const Player& p2){ return this->uuid.compare(p2.uuid) == 0; }
    inline bool operator!=(const Player& p2){ return this->uuid.compare(p2.uuid) != 0; }

    std::string uuid;
    int x;
    int y;
    int velocityX;
    int velocityY;
    int direction;
    std::string atlas;
    std::string frame;
    int width;
    int height;
    std::string role;

};
