#include "GamesManager.h"
#include "controllers/MessageType.h"
#include <trantor/utils/Logger.h>

GamesManager::GamesManager() {
}

GamesManager::~GamesManager() {
	// TODO Auto-generated destructor stub
}


std::string GamesManager::playerHandler(const Json::Value & messageType, const std::string & gameId,const Json::Value & messageJson) {
	std::lock_guard lock {m_mutex};
	Json::Value playerInfos;

	auto gameIt =  m_gameSession.find(gameId);
	if(gameIt == m_gameSession.end()) {
		LOG_ERROR << "An unexistent game is beeing called " << gameId;
		return std::string{};
	}

    if(messageType == MessageType::NEW_PLAYER) {
    	LOG_INFO << "Create player " << messageJson["uuid"].asString();
    	gameIt->second.addNewPlayer(messageJson);
    } else if(messageType == MessageType::PLAYER_MOVED) {
    	gameIt->second.updatePlayerPosition(messageJson);
    }
	// returns JSON with other players informations
	playerInfos = gameIt->second.sendGameState(messageJson["uuid"].asString());
	playerInfos[MESSAGE_TYPE] = MessageType::GAME_STATE;
    return playerInfos.toStyledString();
}

std::string GamesManager::bulletHandler(const Json::Value & messageType, const std::string & gameId, const Json::Value & messageJson) {
	std::lock_guard lock {m_mutex};
	bool isOk;
	Json::Value bulletMessage;

	auto gameIt =  m_gameSession.find(gameId);
	if(gameIt == m_gameSession.end()) {
		LOG_ERROR << "An unexistent game is beeing called " << gameId;
		return std::string{};
	}

	if(messageType == MessageType::NEW_BULLET) {
		LOG_INFO << "Create bullet " << messageJson["uuid"].asString();
		isOk = gameIt->second.addNewBullet(messageJson);
		bulletMessage[MESSAGE_TYPE] = MessageType::NEW_BULLET;

	} else if(messageType == MessageType::BULLET_DESTROY) {
		LOG_INFO << "Delete bullet " << messageJson["uuid"].asString();
		isOk = gameIt->second.destroyBullet(messageJson["uuid"].asString());
		bulletMessage[MESSAGE_TYPE] = MessageType::BULLET_DESTROY;
	}
	if(isOk) {
		bulletMessage[BULLET] = messageJson;
		return bulletMessage.toStyledString();
	} else {
		LOG_ERROR <<"Fail to process bullet";
		return std::string {};
	}
}

std::string GamesManager::gameCreationHandler(const Json::Value & messageType, const std::string & gameId, const Json::Value & messageJson) {

	std::lock_guard lock {m_mutex};
	auto gameIt =  m_gameSession.find(gameId);
	Json::Value responseMessage;
	responseMessage[MESSAGE_TYPE] = MessageType::NEW_GAME;

	if(gameIt != m_gameSession.end()) {
		LOG_ERROR << "An already existent game is being created " << gameId;
		responseMessage["status"] = "KO";
	} else if(messageJson.isMember(GAME_NAME)){
		LOG_INFO << "Creating game " << gameId << " named " << messageJson[GAME_NAME].asString();
		responseMessage["status"] = "OK";
		m_gameSession[gameId] = GameSession{messageJson[GAME_NAME].asString()};
	} else {
		LOG_ERROR << "Unexepected error while creating game " << gameId;
		responseMessage["status"] = "KO";
	}

	return responseMessage.toStyledString();
}

void GamesManager::gameStateHandler(const Json::Value & messageType, const std::string & gameId, const Json::Value & messageJson) {

	std::lock_guard lock {m_mutex};
	auto gameIt = m_gameSession.find(gameId);
	if(gameIt != m_gameSession.end()) {
		LOG_INFO << "A game state is changing " << gameId << " new status " << messageJson[GAME_STATUS].asInt();
		gameIt->second.setGameStatus(GameSession::GameStatus(messageJson[GAME_STATUS].asInt()));
	} else {
		LOG_ERROR << "A non existent game is beeing called " << gameId;
	}

}

void GamesManager::deletePlayerInGame(const std::string & gameId,const std::string & playerId) {

	std::lock_guard lock {m_mutex};
	auto gameIt =  m_gameSession.find(gameId);
	if(gameIt == m_gameSession.end()) {
		LOG_ERROR << "An unexistent game is beeing called " << gameId;
	} else {
		gameIt->second.destroyPlayer(playerId);
	}
}

std::map<std::string, std::string> GamesManager::listCurrentGames() {
	std::map<std::string, std::string> currentGames;
	std::lock_guard lock {m_mutex};
	for (auto const& x : m_gameSession)
	{
		currentGames[x.first] = x.second.getGameName();
	}
	return currentGames;
}
