#include <string>
#include <jsoncpp/json/json.h>

#pragma once

struct Bullet {
	Bullet(const Json::Value & jsonMessage);
	void updateFromJson(const Json::Value & jsonMessage);

    Json::Value toJson();

    inline bool operator==(const Bullet& b2){ return this->uuid.compare(b2.uuid) == 0; }
    inline bool operator!=(const Bullet& b2){ return this->uuid.compare(b2.uuid) != 0; }

    std::string uuid;
    int startX;
    int startY;
    int toX;
    int toY;
};
