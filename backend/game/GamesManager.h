#pragma once
#include <unordered_map>
#include <jsoncpp/json/json.h>

#include "GameSession.h"

class GamesManager {
public:
	GamesManager();
	virtual ~GamesManager();

	std::string playerHandler(const Json::Value & messageType,const  std::string & gameId, const  Json::Value & messageJson);
	std::string bulletHandler(const Json::Value & messageType,const  std::string & gameId, const  Json::Value & messageJson);
	std::string gameCreationHandler(const Json::Value & messageType, const  std::string & gameId, const  Json::Value & messageJson);
	void gameStateHandler(const Json::Value & messageType, const  std::string & gameId, const  Json::Value & messageJson);
	void deletePlayerInGame(const std::string & gameId,const std::string & playerId);
	std::map<std::string, std::string> listCurrentGames();

private:

	std::unordered_map<std::string, GameSession> m_gameSession;
	std::mutex m_mutex;

};
