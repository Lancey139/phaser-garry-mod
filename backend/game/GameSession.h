#pragma once
#include <vector>
#include <mutex>
#include <jsoncpp/json/json.h>
#include <atomic>

#include "Player.h"
#include "Bullet.h"

class GameSession {
	public:

		enum GameStatus {
			LOBBY = 1,
			STARTING = 2,
			PROP_HIDING = 3,
			PLAYING = 4,
			FINISHED = 5
		};

		GameSession(): m_gameName("undefined"){};
		GameSession(const std::string & gameName): m_gameName(gameName){};
		GameSession(const GameSession & gameSession);
		GameSession(GameSession && gameSession);
		GameSession& operator=(const GameSession& gameSession);

    	void addNewPlayer(const Json::Value & jsonMessage);
    	void updatePlayerPosition(const Json::Value & jsonMessage);
    	void destroyPlayer(const std::string & uuid);
    	Json::Value sendGameState(const std::string & uuidToExclude);
    	void setGameStatus(GameStatus newStatus);
    	std::vector<Player> getPlayerVector() { return m_PlayerVector; };

    	bool addNewBullet(const Json::Value & jsonMessage);
		bool destroyBullet(const std::string & uuid);

		std::string getGameName() const {return m_gameName;};

    private:
		void computeGameState();

        std::vector<Player> m_PlayerVector;
        std::vector<Bullet> m_BulletVector;
        std::string m_gameName;
        std::atomic<GameStatus> m_gameStatus = GameStatus::LOBBY;
    	mutable std::mutex m_mutex;
};
