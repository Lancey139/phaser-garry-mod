#include "Player.h"

Player::Player(const Json::Value & jsonMessage) {
	updateFromJson(jsonMessage);
}

void Player::updateFromJson(const Json::Value & jsonMessage) {
	uuid = jsonMessage["uuid"].asString();
	x = jsonMessage["x"].asInt();
	y = jsonMessage["y"].asInt();
	velocityX = jsonMessage["velocityX"].asInt();
	velocityY = jsonMessage["velocityY"].asInt();
	direction = jsonMessage["direction"].asInt();
	atlas = jsonMessage["atlas"].asString();
	frame = jsonMessage["frame"].asString();
	width = jsonMessage["width"].asInt();
	height = jsonMessage["height"].asInt();
	role = jsonMessage["role"].asString();
}

Json::Value Player::toJson() {

	Json::Value returnValue;
	returnValue["uuid"] = uuid;
	returnValue["x"] = x;
	returnValue["y"] = y;
	returnValue["velocityX"] = velocityX;
	returnValue["velocityY"] = velocityY;
	returnValue["direction"] = direction;
	returnValue["atlas"] = atlas;
	returnValue["frame"] = frame;
	returnValue["width"] = width;
	returnValue["height"] = height;
	returnValue["role"] = role;

	return returnValue;
}
