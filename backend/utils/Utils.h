#pragma once

class Utils {
	public:
		Utils();
		virtual ~Utils();
		static size_t select_randomly(size_t start, size_t end);

};
